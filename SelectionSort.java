import java.util.Scanner;

public class SelectionSort {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Arrayin olcusunu daxil edin: ");
        int n = scan.nextInt();
        int arr[] = new int[n];
        System.out.print("Arrayin elementlerini daxil edin: ");
        for (int i = 0; i < n; i++) {
            arr[i] = scan.nextInt();
        }
        SelectionSort selectionSort = new SelectionSort();
        selectionSort.selectionSort(arr,n);

    }

    public void selectionSort(int arr[], int n) {
        for (int i = 0; i < n; i++) {
            int min = i;
            for (int j = i + 1; j < n; j++) {
                if (arr[j] < arr[i]){
                    min = j;
                }
            }
            int eded = arr[min];
            arr[min] = arr[i];
            arr[i] = eded;
        }
        for (int i = 0; i < n; i++) {
            System.out.print(arr[i] + " ");
        }
    }
}
